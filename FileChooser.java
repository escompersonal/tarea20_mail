
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class FileChooser {
    private JFrame frame;
    public FileChooser() {
        frame = new JFrame();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BringToFront();
    }
    public File getFile() {
        JFileChooser fc = new JFileChooser();
        if(JFileChooser.APPROVE_OPTION == fc.showOpenDialog(null)){
            frame.setVisible(false);
           
            return fc.getSelectedFile();

        }else {
            System.out.println("No se selecciono el archivo");
            System.exit(1);
        }
        return null;
    }

    public void close(){
         System.exit(1);
    }  

    private void BringToFront() {                  
            frame.setExtendedState(JFrame.ICONIFIED);
            frame.setExtendedState(JFrame.NORMAL);

    }

}