import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import javax.swing.JOptionPane;
import java.util.Scanner;


public class ServidorMail{
	private File archivo;
	private FileChooser fc;
	private Properties props;
	private  Session session;
	private BodyPart texto;
	private BodyPart adjunto;
	private  MimeMultipart multiParte;
	private MimeMessage message;
	private Transport t;

	
	public void enviar(){
		try{
			
          	fc = new FileChooser();
            props = new Properties();
            Scanner entradaEscaner = new Scanner (System.in);
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "carlosgtz.escom@gmail.com");
            props.setProperty("mail.smtp.auth", "true");
			session = Session.getDefaultInstance(props, null);
            // session.setDebug(true);
            archivo = fc.getFile();

            // Se compone la parte del texto
            texto = new MimeBodyPart();
            texto.setText("Texto del mensaje");

            // Se compone el adjunto con la imagen
            adjunto = new MimeBodyPart();
            adjunto.setDataHandler(new DataHandler(new FileDataSource(archivo)));
            adjunto.setFileName(archivo.getName());

            // Una MultiParte para agrupar texto e imagen.
            multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);

            // Se compone el correo, dando to, from, subject y el
            // contenido.
            message = new MimeMessage(session);
            message.setFrom(new InternetAddress("carlosgtz.escom@gmail.com"));
            System.out.print("Correo destinatario: ");
            String destinatario = entradaEscaner.nextLine (); 
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(destinatario));
            System.out.print("Asunto del correo: ");
            String asunto = entradaEscaner.nextLine ();
            message.setSubject(asunto);
            message.setContent(multiParte);

            // Se envia el correo.
            t = session.getTransport("smtp");
            t.connect("carlosgtz.escom@gmail.com", "2014630202;");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            fc.close();
        }catch(Exception e){

        }
	}
	
}